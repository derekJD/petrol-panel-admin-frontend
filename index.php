<?php include 'head.php' ?>

<script>
  $(function() {
  var total = 0, enteredAmount, itemCount = 0;

  $('#button').click(calculate);
  function printTotal() {
    $('#total').text(total.toFixed(2) + ' zł');
  }
  function clearVal() {
    $('#newEntry').val("");
  }
  function clearEntries() {
    $('#listProduct').html("");
    $('#listProduct2').html("");
  }

  function addToList() {
    itemName = $('#theSelect option:selected').text();
    itemPrice = parseFloat($('#price').text());
    
    $('#listProduct').append('<tr><td>  ' + itemName + '</td><td>' + itemCount+ '</td><td> ' + itemPrice + ' </td><td>'+ enteredAmount.toFixed(2) + ' zł</tr>');
    $('#listProduct2').append('<tr><td><label><input type="checkbox" checked>  ' + itemName + '</td><td>' + itemCount+ '</td><td> ' + itemPrice + ' </label></td><td>'+ enteredAmount.toFixed(2) + ' zł</tr>');
  }

  function calculate(event) {
  	itemCount=parseFloat($('#newEntry').val());
    event.preventDefault();
    enteredAmount = parseFloat($('#newEntry').val())*parseFloat($('#price').text());
    if (isNaN(enteredAmount)) {
      enteredAmount = 0;
    }
    total = total + enteredAmount;
    printTotal();
    clearVal();
    if (enteredAmount > 0) {
      addToList();
    }

  }

  $(document).bind('keypress', function(event) {
      if( event.which === 67 && event.shiftKey ) {
          event.preventDefault();
          total = 0;
          itemCount = 0;
          printTotal();
          clearEntries();
      }
  });

  
  //table to array
  $("#closeReceipt").on("click", function(){
	var data = Array(); //array z lista produktow d[i] << zwraca caly rzad tabeli,
	//np data[0][0] zwroci nazwe pierwszego produktu, data[0][1] zwroci ilosc itp;
	$("#listProduct2 tr").each(function(i, v){
	    data[i] = Array();
	    $(this).children('td').each(function(ii, vv){
	        data[i][ii] = $(this).text();
	    }); 
	})
	// tu masz przyklady:
	alert("rzad1: " + data[0][2] +"\n");
	alert("rzad2: " + data[1][0] +"\n");
	alert("rzad3: " + data[2][3] +"\n");
        
});



});
  
</script>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="card mb-3">


					<div class="card-header">
						<h3><i class="fas fa-receipt"></i> Rachunek:</h3>
					</div>
	
					<div class="card-body">


											<div class="table-responsive">
											<table class="table table-bordered table-hover">
											<thead>
											<tr>
												
												<th>Produkt</th>
												<th style="width:150px">Ilość</th>
												<th style="width:130px">Aktualna cena</th>
												<th style="width:150px">Cena za produkt</th>
											  </tr>
											</thead>
											<tfoot>
											<tr>
												
												<th colspan="2" class="table-borderless"></th>
												<th colspan="2">Razem: <p id="total"></p>
												
												<span class="pull-right"><button id="invoice" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_add_invoice"><i class="fas fa-file-invoice" aria-hidden="true"></i> Wystaw rachunek</button></span>
												<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_add_invoice" aria-hidden="true" id="modal_add_invoice">

											<div class="modal-dialog">
												<!--add user modal -->
												<div class="modal-content">

													<form action="#" method="post" enctype="multipart/form-data">


													<div class="modal-header">
													<h5 class="modal-title">Wystawianie rachunku</h5>
													<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
													</div>
													<div class="table-responsive">
											<table class="table table-bordered table-hover">
											<thead>
											<tr>
												
												<th>Produkt</th>
												<th >Ilość</th>
												<th>Aktualna cena</th>
												<th>Cena za produkt</th>
											 </tr>
											</thead>
											<tbody id="listProduct2">
											
											</tbody>

											</table>
											</div>



													<div class="modal-body">
			
														<div class="row">
															<div class="col-xs-12">
															<div class="form-group">
		                                                    <label>Rodzaj rachunku<span class="text-danger">*</span></label>
		                                                    <button id="radioReceipt" type="button" class="btn btn-primary">Paragon</button>
		                                                    <button id="radioInvoice" type="button" class="btn btn-primary">Faktura</button>
		                                                    </div>
		                                                    </div>
															<div id="formForInvoice" class="col-xs-12">
															<div class="form-group">
		                                                    <label>Rodzaj klienta<span class="text-danger">*</span></label>
		                                                    <button id="radioPrivate" type="button" class="btn btn-primary">Osoba prywatna</button>
		                                                    <button id="radioCompany" type="button" class="btn btn-primary">Firma</button>
		                                                    </div>
		                                                    </div>
															<div id="form1">
															 <!-- tu trza wjebac selecta z klientami z bazy do faktury -->
															<div class="row">
																<div class="col-md-6">
					                                        	<div class="form-group">
					                                            <label for="userName">Imię<span class="text-danger">*</span></label>
					                                            <input type="text" name="nick" data-parsley-trigger="change" required placeholder="Wprowadź imię" class="form-control" id="userName">
					                                            </div>
					                                            </div>
					                                            <div class="col-md-6">
					                                        	<div class="form-group">
					                                            <label for="userLastName">Nazwisko<span class="text-danger">*</span></label>
					                                            <input type="text" name="userLastName" data-parsley-trigger="change" required placeholder="Wprowadź nazwisko" class="form-control" id="userLastName">
					                                        	</div>
					                                        	</div>
					                                    	</div>
					                                    	<div class="row">
																<div class="col-md-8">
					                                        	<div class="form-group">
					                                            <label for="userNameStreet">Ulica<span class="text-danger">*</span></label>
					                                            <input type="text" name="userNameStreet" data-parsley-trigger="change" required placeholder="Wprowadź ulicę" class="form-control" id="userNameStreet">
					                                            </div>
					                                            </div>
					                                            <div class="col-md-4">
					                                        	<div class="form-group">
					                                            <label for="userNumberStreet">Numer<span class="text-danger">*</span></label>
					                                            <input type="text" name="nick" data-parsley-trigger="change" required placeholder="Numer" class="form-control" id="userNumberStreet">
					                                        	</div>
					                                        	</div>
					                                    	</div>
															<div class="row">
																<div class="col-md-12">
					                                        	<div class="form-group">
					                                            <label for="userEmailAddress">Email adres<span class="text-danger">*</span></label>
					                                            <input type="email" name="email" data-parsley-trigger="change" required placeholder="Wprowadź e-mail" class="form-control" id="userEmailAddress">
					                                            </div>
					                                            </div>	                                            
					                                    	</div>
					                                      

					                                        
															</div>


				                                <!--Form for company -->
				                                    <div id="form2" action="#" data-parsley-validate novalidate>
				                                    	<!-- tu trza wjebac selecta z klientami z bazy do faktury -->
														<div class="row">
																<div class="col-md-7">
					                                        	<div class="form-group">
					                                            <label for="companyName">Nazwa firmy<span class="text-danger">*</span></label>
					                                            <input type="text" name="nick" data-parsley-trigger="change" required placeholder="Wprowadź imię" class="form-control" id="companyName">
					                                            </div>
					                                            </div>
					                                            <div class="col-md-5">
					                                        	<div class="form-group">
					                                            <label for="companyLastName">NIP<span class="text-danger">*</span></label>
					                                            <input type="text" name="companyLastName" data-parsley-trigger="change" required placeholder="Wprowadź NIP" class="form-control" id="companyLastName">
					                                        	</div>
					                                        	</div>
					                                    	</div>
					                                    	<div class="row">
																<div class="col-md-8">
					                                        	<div class="form-group">
					                                            <label for="companyNameStreet">Ulica<span class="text-danger">*</span></label>
					                                            <input type="text" name="companyNameStreet" data-parsley-trigger="change" required placeholder="Wprowadź ulicę" class="form-control" id="companyNameStreet">
					                                            </div>
					                                            </div>
					                                            <div class="col-md-4">
					                                        	<div class="form-group">
					                                            <label for="companyNumberStreet">Numer<span class="text-danger">*</span></label>
					                                            <input type="text" name="nick" data-parsley-trigger="change" required placeholder="Numer" class="form-control" id="companyNumberStreet">
					                                        	</div>
					                                        	</div>
					                                    	</div>
															<div class="row">
																<div class="col-md-12">
					                                        	<div class="form-group">
					                                            <label for="companyEmailAddress">Email adres<span class="text-danger">*</span></label>
					                                            <input type="email" name="email" data-parsley-trigger="change" required placeholder="Wprowadź e-mail" class="form-control" id="companyEmailAddress">
					                                            </div>
					                                            </div>	                                            
					                                    	</div>

				                                       

		     		                     			  </div><!--end of company-->
		     		                     			  <div class="col-lg-4 offset-lg-8 col-xs-12">
		     		                     			  <div class="form-group">
					                                            <button id="closeReceipt" class="btn btn-primary" >
					                                                Zatwierdź
					                                            </button>
					                                  </div>
					                              	  </div>

												</div> <!-- end add company modal-->
											</div>
										</div> <!--modal fad-custom-->

												</th>
												
											  </tr>
											</tfoot>
											<tbody id="listProduct">
											
											</tbody>
											</table>
											</div>


					</div>

		</div>
	</div>
</div>
<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

								<div class="card mb-3">

									<div class="card-header">
										<h3><i class="fa fa-user"></i> Kasa:</h3>
									</div>
									<!-- end card-header -->

									<div class="card-body">


											<div class="table-responsive">
											<table class="table">
											<thead>
											<tr>
												
												<th style="width:240px">Produkt</th>
												<th>Aktualna cena</th>
												<th>Ilość</th>
											  </tr>
											</thead>
											<tbody>
											<tr >
												<form id="entry">
													<td><select class="form-control select2 col-lg-12" id="theSelect" name="state"> 
												    	<option value="4.2">PB 95</option>
												    	<option value="5">ON</option>
												    	<option value="4,7">PB98</option>
												    	<option value="5.2">ON Super</option>
												    	<option value="1.4">Batonik</option>
												    	<option value="3.99">Banan</option>
												    	<option value="2.5">Piwo</option>
												 	</select>
												 	</td>
													<td>
														<strong id="price"></strong>
													</td>
													<td><input id="newEntry" autofocus placeholder="Jaka ilość?"> <a href="#" class="btn btn-primary btn-sm" id="button"><i class="fas fa-plus"></i></a></td>
											
													
												</form>
													<!--end edit and remove-->
											</tr>
											</tbody>
											</table>
											</div>


									</div>
									<!-- end card-body -->

								</div>
								<!-- end card -->

							</div>
							<!-- end col -->

</div>
<!-- end row -->

<?php include 'footer.php' ?>
