<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Petrol Admin</title>
        <meta name="description" content="Petrol PAnel">
        <meta name="author" content="Pike Web Development - https://www.pikephp.com">
<script src="http://localhost:35729/livereload.js"></script>
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- Switchery css -->
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- Font Awesome CSS -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Custom CSS -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/plugins/datetimepicker/js/moment-with-locales.js"></script>
        <!-- BEGIN CSS for this page -->
<!-- BEGIN CSS for this page -->
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<!-- END CSS for this page -->
        <!-- END CSS for this page -->

</head>

<body class="adminbody">

<div id="main">

    <!-- top bar navigation -->
    <div class="headerbar">

        <!-- LOGO -->
        <div class="headerbar-left">
            <a href="index.html" class="logo"><img alt="logo" src="assets/images/logo.png" /> <span>PETROL</span></a>
        </div>

        <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">







                        <li class="list-inline-item dropdown notif">
                            <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/avatars/admin.png" alt="Profile image" class="avatar-rounded">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Hello, admin</small> </h5>
                                </div>

                                <!-- item-->
                                <a href="#" class="dropdown-item notify-item">
                                    <i class="fa fa-power-off"></i> <span>Wyloguj</span>
                                </a>
                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left">
                                <i class="fa fa-fw fa-bars"></i>
                            </button>
                        </li>
                    </ul>

        </nav>

    </div>
    <!-- End Navigation -->


    <!-- Left Sidebar -->
    <div class="left main-sidebar">

        <div class="sidebar-inner leftscroll">

            <div id="sidebar-menu">

            <ul>
                <!--Szef Menu Start-->
                    <li class="submenu">
                        <a href="index.html"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                    </li>

                    <li class="submenu">
                        <a href="#"><i class="fa fa-users"></i><span>Zarządzaj pracownikami </span> </a>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fas fa-coins"></i><span> Zmień ceny </span> </a>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fas fa-warehouse"></i><span> Składanie zamówień </span> </a>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fas fa-cog"></i></i><span>Klienci</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="#">Dodaj klienta</a></li>
                                <li><a href="#">Zmień dane klienta</a></li>
                            </ul>
                    </li>
                 <!--Szef Menu End -->
                 <!--Pracownik Menu Start -->
                    <li class="submenu">
                        <a href="#"><i class="fas fa-car"></i><span> Rezerwacja myjni</span> </a>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fa fa-id-card"></i><span>Załóż kartę </span> </a>
                    </li>
                    <li class="submenu">
                        <a href="#"><i class="fas fa-cash-register"></i><span>Kasa fiskalna </span> </a>
                    </li>
                    <!--Pracownik Menu End-->
                    <!--Pracownik Uprawiony Menu Start-->

                    <li class="submenu">
                        <a href="#"><i class="fas fa-tools"></i></i></i><span>Zarządzanie stacją</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="#">Monitoruj instalacje</a></li>
                                <li><a href="#">Tryb maintance</a></li>
                                <li><a href="#">Edytuj stan paliwa</a></li>
                            </ul>
                    </li>

                    <!--Pracownik Uprawiony Menu End-->
            </ul>

            <div class="clearfix"></div>

            </div>

            <div class="clearfix"></div>

        </div>

    </div>
    <!-- End Sidebar -->


    <div style="min-height: 100vh;" class="content-page">

        <!-- Start content -->
        <div  class="content">

            <div class="container-fluid">
